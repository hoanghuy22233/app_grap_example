import 'package:app_grap_example/src/app.dart';
import 'package:app_grap_example/src/blocs/auth_bloc.dart';
import 'package:app_grap_example/src/resources/home_page.dart';
import 'package:app_grap_example/src/resources/login_page.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp(
      new AuthBloc(),


      MaterialApp(
        home: LoginPage(),
      )));
}
